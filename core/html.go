package core

import (
    "code.google.com/p/go-html-transform/h5"
    "code.google.com/p/go.net/html"
    "io"
    "strings"
)

func Any(vs []html.Attribute, f func(html.Attribute) bool) bool {
    for _, v := range vs {
        if f(v) {
            return true
        }
    }
    return false
}

func First(vs []html.Attribute, f func(html.Attribute) bool) *html.Attribute {
    for _, v := range vs {
        if f(v) {
            return &v
        }
    }
    return nil
}

func isItemLink(attr html.Attribute) bool {
    return attr.Key == "class" && strings.Contains(attr.Val, "item_link")
}

func isHref(attr html.Attribute) bool {
    return attr.Key == "href"
}

func parseAds(r io.Reader) ([]Ad, error) {
    tree, err := h5.New(r)

    if err != nil {
        return nil, err
    }

    ads := make([]Ad, 0)
    tree.Walk(func(n *html.Node) {
        if n.Type == html.ElementNode && n.Data == "a" && Any(n.Attr, isItemLink) {
            hrefElement := First(n.Attr, isHref)
            if hrefElement != nil {
                uri := hrefElement.Val
                title := n.FirstChild.Data
                ad := Ad{Title: title, Uri: uri}
                ads = append(ads, ad)
            }
        }
    })

    return ads, nil
}
