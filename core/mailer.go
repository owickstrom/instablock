package core

import (
    "log"
    "net/smtp"
)

type Mailer func(string, string, string) error

func CreateMailer(host, email, password string) Mailer {
    auth := smtp.PlainAuth("", email, password, host)

    return func(recipient, subject, body string) error {
        log.Println("Sending mail to", recipient)

        headers := "From: " + email + "\r\n"
        headers = headers + "MIME-Version: 1.0\r\n"
        headers = headers + "Content-Type: text/html; charset=ISO-8859-1\r\n"
        headers = headers + "Subject: " + subject + "\r\n"
        err := smtp.SendMail(
            host+":587",
            auth,
            email,
            []string{recipient},
            []byte(headers+"\r\n"+body),
        )
        if err != nil {
            return err
        } else {
            log.Println("Sent mail to", recipient)
            return nil
        }
    }
}
