package core

import (
    "fmt"
    "log"
    "net/http"
    "strings"
    "time"
)

type Ad struct {
    Title string
    Uri   string
}

func (a Ad) String() string {
    return a.Title
}

type Tracker interface {
    FilterNew(*Subscription, []Ad) []Ad
    MarkAsOld(*Subscription, Ad) error
}

type Subscription struct {
    Subscriber string
    Feed       chan Ad
    City       string
    Query      string
    Tracker    Tracker
}

func NewSubscription(subscriber string, feed chan Ad, city string, query string, tracker Tracker) *Subscription {
    return &Subscription{
        subscriber,
        feed,
        city,
        query,
        tracker,
    }
}

func getCurrentAds(city string, query string) ([]Ad, error) {
    city = strings.ToLower(city)
    query = strings.Replace(query, " ", "+", -1)
    uri := fmt.Sprintf("http://www.blocket.se/%s?q=%s", city, query)

    log.Println("GET", uri)

    resp, err := http.Get(uri)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    return parseAds(resp.Body)
}

func lookForNewAds(s *Subscription) {
    results, err := getCurrentAds(s.City, s.Query)
    if err != nil {
        log.Fatalf("Failed to get current ads: %s\n", err)
    }
    filteredNewAds := s.Tracker.FilterNew(s, results)
    for _, newAd := range filteredNewAds {
        s.Feed <- newAd
        s.Tracker.MarkAsOld(s, newAd)
    }
}

func LookForNewAdsPeriodically(s *Subscription) {
    ticker := time.NewTicker(time.Second * 10)
    go func() {
        for _ = range ticker.C {
            lookForNewAds(s)
        }
    }()
}

func pluralize(num int, term string) string {
    if num == 1 {
        return term
    } else {
        return term + "s"
    }
}

func notifySubscriberOf(s *Subscription, mailer Mailer, ads []Ad) {
    subject := fmt.Sprintf("Instablock update for '%s'", s.Query)
    numAds := len(ads)
    adTerm := pluralize(numAds, "ad")
    body := fmt.Sprintf("<h2>You have %d new %s!</h2>\r\n<ol>\r\n", numAds, adTerm)

    for _, ad := range ads {
        body += fmt.Sprintf("<li><a href=\"%s\">%s</a></li>\r\n", ad.Uri, ad.Title)
    }

    body += "</ol>\r\n"

    err := mailer(s.Subscriber, subject, body)
    if err != nil {
        log.Fatalf("Failed to send mail to %s\n", s.Subscriber)
    }
}

func NotifySubscriberWhenSuitable(s *Subscription, mailer Mailer) {
    pendingNewAds := make([]Ad, 0)

    addToPending := func(newAd Ad) {
        pendingNewAds = append(pendingNewAds, newAd)
    }

    sendAndClearPending := func() {
        notifySubscriberOf(s, mailer, pendingNewAds)
        pendingNewAds = make([]Ad, 0)
    }

    for {
        select {
        case newAd := <-s.Feed:
            addToPending(newAd)
            if len(pendingNewAds) >= 20 {
                sendAndClearPending()
            }
        case <-time.After(time.Minute * 1):
            if len(pendingNewAds) > 0 {
                sendAndClearPending()
            }
        }

    }
}
