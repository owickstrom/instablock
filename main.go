package main

import (
    ic "bitbucket.org/OskarWickstrom/instablock/core"
    im "bitbucket.org/OskarWickstrom/instablock/mongo"
    "bufio"
    "fmt"
    "os"
    "strings"
)

func getInput(reader *bufio.Reader, name string) string {
    var input string
    for {
        fmt.Printf("Enter %s: ", name)
        input, _ = reader.ReadString('\n')
        if len(strings.TrimSpace(input)) > 0 {
            break
        } else {
            fmt.Printf("%s cannot be empty!\n", name)
        }
    }
    return input
}

func getSettings() (string, string, string, string, string) {
    reader := bufio.NewReader(os.Stdin)

    email := getInput(reader, "sender email")
    password := getInput(reader, "sender password")
    city := getInput(reader, "city")
    query := getInput(reader, "query")
    subscriber := getInput(reader, "subscriber email")

    return strings.TrimSpace(email),
        strings.TrimSpace(password),
        strings.TrimSpace(city),
        strings.TrimSpace(query),
        strings.TrimSpace(subscriber)
}

func run() {
    feed := make(chan ic.Ad, 1)
    tracker, err := im.NewMongoDbTracker("localhost")

    if err != nil {
        fmt.Fprintf(os.Stderr, "Failed to connect to mongo: %s\n", err)
        os.Exit(1)
    }
    defer tracker.Close()

    email, password, city, query, subscriber := getSettings()
    mailer := ic.CreateMailer("smtp.live.com", email, password)
    subscription := ic.NewSubscription(subscriber, feed, city, query, tracker)

    go ic.LookForNewAdsPeriodically(subscription)
    ic.NotifySubscriberWhenSuitable(subscription, mailer)
}

func main() {
    run()
}
