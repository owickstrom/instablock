package mongo

import (
    ic "bitbucket.org/OskarWickstrom/instablock/core"
    "fmt"
    "testing"
)

var (
    car  = ic.Ad{Title: "Car", Uri: "http://www.blocket.se/the-car"}
    boat = ic.Ad{Title: "Boat", Uri: "http://www.blocket.se/the-boat"}
    dog  = ic.Ad{Title: "Dog", Uri: "http://www.blocket.se/the-dog"}
    ads  = []ic.Ad{car, boat, dog}
)

func assertEquals(t *testing.T, a []ic.Ad, b []ic.Ad) {
    if len(a) != len(b) {
        t.Error(fmt.Sprintf("%s", a), "!=", fmt.Sprintf("%s", b))
        return
    }

    for i, one := range a {
        other := b[i]
        if one != other {
            t.Error("Expected to find", one, "in", b)
            return
        }
    }
}

func setup(tracker *MongoDbTracker) {
    tracker.C.DropCollection()
}

func TestTrackerWithNoneMarkedAsOld(t *testing.T) {
    tracker, err := NewMongoDbTracker("localhost", "test@test.org")
    if err != nil {
        t.Error(err)
    }
    setup(tracker)
    defer tracker.Close()

    filtered := tracker.FilterNew(ads)

    assertEquals(t, filtered, ads)
}

func TestTrackerWithOneMarkedAsOld(t *testing.T) {
    tracker, err := NewMongoDbTracker("localhost", "test@test.org")
    if err != nil {
        t.Error(err)
    }
    setup(tracker)
    defer tracker.Close()

    err = tracker.MarkAsOld(car)
    if err != nil {
        t.Error(err)
    }

    withoutCar := []ic.Ad{boat, dog}
    filtered := tracker.FilterNew(ads)

    assertEquals(t, filtered, withoutCar)
}
