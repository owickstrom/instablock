package mongo

import (
    ic "bitbucket.org/OskarWickstrom/instablock/core"
    "encoding/base64"
    "labix.org/v2/mgo"
    "labix.org/v2/mgo/bson"
    "log"
)

type MongoDbTracker struct {
    Session *mgo.Session
}

func (m MongoDbTracker) Close() {
    m.Session.Close()
}

func (t *MongoDbTracker) getTrackerFor(s *ic.Subscription) *mgo.Collection {
    subscriberColl := base64.StdEncoding.EncodeToString([]byte(s.Subscriber))
    return t.Session.DB("instablock").C("tracker" + subscriberColl)
}

func (t *MongoDbTracker) FilterNew(s *ic.Subscription, ads []ic.Ad) []ic.Ad {
    filtered := make([]ic.Ad, 0)
    c := t.getTrackerFor(s)
    for _, ad := range ads {
        query := bson.M{"uri": ad.Uri}
        count, err := c.Find(query).Count()
        if err != nil || count == 0 {
            filtered = append(filtered, ad)
        }
    }
    return filtered
}

func (t *MongoDbTracker) MarkAsOld(s *ic.Subscription, ad ic.Ad) error {
    c := t.getTrackerFor(s)
    err := c.Insert(bson.M{"uri": ad.Uri})
    if err != nil {
        return err
    }
    log.Println("Marked", ad.Title, "as old")
    return nil
}

func NewMongoDbTracker(host string) (*MongoDbTracker, error) {
    session, err := mgo.Dial(host)
    if err != nil {
        return nil, err
    }
    return &MongoDbTracker{session}, nil
}
