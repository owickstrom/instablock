# Instablock

Instablock runs your queries on blocket.se and notifies you when something new pops up.

### Prerequisites

* Go with `$GOPATH` set up.
* MongoDB running on `localhost:27017` with guest/guest credentials.

### Install and Run

    $ go get bitbucket.org/OskarWickstrom/instablock
    $ go install bitbucket.org/OskarWickstrom/instablock
    $ instablock # you will be prompted for some input...
